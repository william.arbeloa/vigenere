<?php
include 'utils.php';

try {
    $conn = connectDb();
    $error = '';

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $username = $_POST['username'];
        $password = password_hash($_POST['password'], PASSWORD_BCRYPT);

        // Vérifie que la saisie n'est pas nulle
        if (empty($username) || empty($password)) {
            $error = "Il faut entrer un nom d'utilisateur et un mot de passe.";
        }
        // Vérifie si le nom d'utilisateur existe déjà
        elseif (userExists($username, $conn)) {
            $error = "Le nom d'utilisateur est déjà pris !";
        } else {
            // Insérer un nouvel utilisateur
            $stmt = $conn->prepare("INSERT INTO users (username, password) VALUES (?, ?)");
            $stmt->bind_param("ss", $username, $password);

            if ($stmt->execute()) {
                header('Location: login.php');
                exit();
            } else {
                throw new Exception("Error: " . $stmt->error);
            }
            $stmt->close();
        }
    }
    $conn->close();
} catch (Exception $e) {
    $error = $e->getMessage();
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Inscription</title>
    <script>
    function validateForm() {
        var username = document.getElementById("username").value;
        var password = document.getElementById("password").value;
        if (username === "" || password === "") {
            alert("Il faut entrer un nom d'utilisateur et un mot de passe.");
            return false;
        }
        return true;
    }
    </script>
</head>
<body>
    <h1>Inscription</h1>
    <?php if ($error) : ?>
        <p style="color: red;"><?php echo $error; ?></p>
    <?php endif; ?>
    <form action="register.php" method="POST" onsubmit="return validateForm()">
        <label for="username">Nom d'utilisateur : </label><br>
        <input type="text" id="username" name="username" required><br>
        <label for="password">Mot de passe : </label><br>
        <input type="password" id="password" name="password" required><br>
        <input type="submit" value="Inscription">
    </form>
    <p>Déjà inscrit ? <a href="login.php">Connectez-vous ici</a></p>
</body>
</html>
