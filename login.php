<?php
include 'utils.php';

$error = '';

try {
    $conn = connectDb();

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $username = $_POST['username'];
        $password = $_POST['password'];


        // Vérifie que la saisie n'est pas nulle
        if (empty($username) || empty($password)) {
            $error = "Il faut entrer un nom d'utilisateur et un mot de passe.";
            // Sinon on execute la requête
        } else {
            $stmt = $conn->prepare("SELECT id, password FROM users WHERE username = ?");
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $stmt->store_result();
            
            if ($stmt->num_rows > 0) {
                $stmt->bind_result($id, $hashed_password);
                $stmt->fetch();
                
                if (password_verify($password, $hashed_password)) {
                    session_start();
                    $_SESSION['user_id'] = $id;
                    $_SESSION['username'] = $username;
                    header('Location: index.php');
                    exit();
                } else {
                    $error = "Mot de passe incorrect.";
                }
            } else {
                $error = "Ce nom d'utilisateur n'existe pas.";
            }
            
            $stmt->close();
        }
    }

    $conn->close();
} catch (Exception $e) {
    $error = $e->getMessage();
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Connexion</title>
    <script>
    function validateForm() {
        var username = document.getElementById("username").value;
        var password = document.getElementById("password").value;
        if (username === "" || password === "") {
            alert("Il faut entrer un nom d'utilisateur et un mot de passe.");
            return false;
        }
        return true;
    }
    </script>
</head>
<body>
    <h1>Connexion</h1>
    <?php if ($error) : ?>
        <p style="color: red;"><?php echo $error; ?></p>
    <?php endif; ?>
    <form action="login.php" method="POST" onsubmit="return validateForm()">
        <label for="username">Nom d'utilisateur :</label><br>
        <input type="text" id="username" name="username" required><br>
        <label for="password">Mot de Passe:</label><br>
        <input type="password" id="password" name="password" required><br>
        <input type="submit" value="Connexion">
    </form>
    <p>Pas encore inscrit ? <a href="register.php">Inscrivez-vous ici</a></p>
</body>
</html>
