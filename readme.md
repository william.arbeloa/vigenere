# Chiffre de Vigenère

## Description
Cette application web implémente le chiffrement de Vigenère. Les utilisateurs peuvent s'inscrire, se connecter, chiffrer et déchiffrer des messages, et consulter l'historique de leurs messages.

## Prérequis
- PHP >= 7.4
- MySQL >= 5.7
- Apache/Nginx
- phpMyAdmin (optionnel mais recommandé)

## Installation

### Étape 1 : Cloner le dépôt
Clonez ce dépôt sur votre serveur local ou votre hébergement web :
```bash
git clone https://gitlab.com/william.arbeloa/vigenere.git
```

### Étape 2 : Configuration de la base de données

## Créer la base de données et les tables :

Connectez-vous à votre serveur MySQL et exécutez les commandes SQL suivantes pour créer la base de données et les tables nécessaires :

```sql
CREATE DATABASE vigenere_db;
USE vigenere_db;

CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(50) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL
);

CREATE TABLE messages (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    message TEXT NOT NULL,
    encrypted_message TEXT NOT NULL,
    timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users(id)
);
```

### Étape 3 : Configuration de l'application
## Configurer la connexion à la base de données :

Modifiez les paramètres de connexion à la base de données dans la fonction `connectDb()` de utils.php.
Le cas d'exemple est pour l'utilisateur "root", qui n'a pas de mot de passe.
```php
$conn = new mysqli("localhost", "root", "", "vigenere_db");
```

## Assurez-vous que les permissions des fichiers soient correctes :

Les fichiers PHP doivent être accessibles par le serveur web, et les permissions doivent permettre l'exécution de ces fichiers.

### Étape 4 : Lancer l'application

## Démarrer le serveur web :
Assurez-vous que votre serveur web (Apache/Nginx) est en cours d'exécution.

## Accéder à l'application :
Ouvrez votre navigateur et accédez à `http://localhost/vigenere/` pour voir la page de connexion.

### Utilisation

## Inscription
- Accédez à `http://localhost/vigenere/register.php`.
- Remplissez le formulaire d'inscription avec un nom d'utilisateur et un mot de passe.
- Cliquez sur "Inscription" pour créer un nouveau compte.
- L'utilisateur et le mot de passe ne peuvent être vides.

## Connexion 

- Accédez à `http://localhost/vigenere/login.php`.
- Remplissez le formulaire de connexion avec votre nom d'utilisateur et votre mot de passe.
- Cliquez sur "Connexion" pour vous connecter.
- L'utilisateur et le mot de passe ne peuvent être vides.

## Utilisation de l'application
- Une fois connecté, vous serez redirigé vers index.php.
- Utilisez le formulaire pour entrer un message et une clef de chiffrage, puis cliquez sur "Chiffrer" ou "Déchiffrer".
- Consultez l'historique des messages en bas de la page.

## Déconnexion
- Cliquez sur le lien "Déconnexion" en haut de la page pour vous déconnecter.

## Gestion des erreurs
- Les erreurs critiques sont gérées à l'aide de blocs `try-catch` pour assurer une meilleure robustesse et une meilleure expérience utilisateur.


## Développement 

## Structure des fichiers
- index.php : Page principale pour chiffrer et déchiffrer des messages.
- login.php : Page de connexion.
- register.php : Page d'inscription.
- logout.php : Script de déconnexion.
- process.php : Script pour traiter les demandes AJAX de chiffrement et déchiffrement.
- history.php : Script pour afficher l'historique des messages.
- utils.php : Fichier contenant les fonctions utilitaires.