<?php
session_start();
include 'utils.php';

$conn = connectDb();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $message = $_POST['message'];
    $key = $_POST['key'];
    $action = $_POST['action'];
    $user_id = $_SESSION['user_id'];

    if ($action == 'encrypt') {
        $encryptedMessage = vigenereEncrypt($message, $key);
        $stmt = $conn->prepare("INSERT INTO messages (user_id, message, encrypted_message) VALUES (?, ?, ?)");
        $stmt->bind_param("iss", $user_id, $message, $encryptedMessage);
        $stmt->execute();
        echo $encryptedMessage;
    } elseif ($action == 'decrypt') {
        $decryptedMessage = vigenereDecrypt($message, $key);
        echo $decryptedMessage;
    }
}

$conn->close();
?>
