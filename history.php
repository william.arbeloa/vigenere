<?php
session_start();
include 'utils.php';

$conn = connectDb();

$user_id = $_SESSION['user_id'];
$stmt = $conn->prepare("SELECT message, encrypted_message, timestamp FROM messages WHERE user_id = ? ORDER BY timestamp DESC");
$stmt->bind_param("i", $user_id);
$stmt->execute();
$result = $stmt->get_result();

echo "<table border='1'>
<tr>
<th>Message</th>
<th>Message chiffré</th>
<th>Timestamp</th>
</tr>";

while ($row = $result->fetch_assoc()) {
    echo "<tr>";
    echo "<td>" . htmlspecialchars($row['message']) . "</td>";
    echo "<td>" . htmlspecialchars($row['encrypted_message']) . "</td>";
    echo "<td>" . $row['timestamp'] . "</td>";
    echo "</tr>";
}

echo "</table>";

$conn->close();
?>
