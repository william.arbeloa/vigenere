<?php
session_start();
if (!isset($_SESSION['user_id'])) {
    header('Location: login.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Chiffre de Vigenère</title>
    <style>
        /* Ajoutez ici votre CSS pour le style */
    </style>
</head>
<body>
    <h1>Chiffre de Vigenère</h1>
    <p>Bienvenue, <?php echo htmlspecialchars($_SESSION['username']); ?>! <a href="logout.php">Déconnexion</a></p>
    <form id="cipherForm">
        <label for="message">Message:</label><br>
        <input type="text" id="message" name="message"><br>
        <label for="key">Clef:</label><br>
        <input type="text" id="key" name="key"><br>
        <input type="button" value="Chiffrer" onclick="processMessage('encrypt')">
        <input type="button" value="Déchiffrer" onclick="processMessage('decrypt')">
    </form>
    <p id="result"></p>

    <h2>Historique des messages</h2>
    <div id="history"></div>

    <script>
        function processMessage(action) {
            var message = document.getElementById('message').value;
            var key = document.getElementById('key').value;

            var xhr = new XMLHttpRequest();
            xhr.open("POST", "process.php", true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    document.getElementById('result').innerText = xhr.responseText;
                    loadHistory();
                }
            };

            xhr.send("message=" + encodeURIComponent(message) + "&key=" + encodeURIComponent(key) + "&action=" + action);
        }

        function loadHistory() {
            var xhr = new XMLHttpRequest();
            xhr.open("GET", "history.php", true);

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    document.getElementById('history').innerHTML = xhr.responseText;
                }
            };

            xhr.send();
        }

        // Charge history lorsque la page charge
        loadHistory();
    </script>
</body>
</html>
