<?php

function connectDb() {
    try {
        $conn = new mysqli("localhost", "root", "", "vigenere_db");

        if ($conn->connect_error) {
            throw new Exception("Connection failed: " . $conn->connect_error);
        }

        return $conn;
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        exit();
    }
}

function userExists($username, $conn) {
    $stmt = $conn->prepare("SELECT id FROM users WHERE username = ?");
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $stmt->store_result();
    
    $exists = $stmt->num_rows > 0;
    
    $stmt->close();
    
    return $exists;
}

function vigenereEncrypt($plaintext, $key) {
    $key = strtoupper($key);
    $keyLength = strlen($key);
    $ciphertext = '';

    for ($i = 0, $j = 0; $i < strlen($plaintext); $i++) {
        $char = strtoupper($plaintext[$i]);

        if (ctype_alpha($char)) {
            $shift = ord($key[$j % $keyLength]) - ord('A');
            $cipherChar = chr(((ord($char) - ord('A') + $shift) % 26) + ord('A'));
            $j++;
        } else {
            $cipherChar = $char;
        }

        $ciphertext .= $cipherChar;
    }

    return $ciphertext;
}

function vigenereDecrypt($ciphertext, $key) {
    $key = strtoupper($key);
    $keyLength = strlen($key);
    $plaintext = '';

    for ($i = 0, $j = 0; $i < strlen($ciphertext); $i++) {
        $char = strtoupper($ciphertext[$i]);

        if (ctype_alpha($char)) {
            $shift = ord($key[$j % $keyLength]) - ord('A');
            $plainChar = chr(((ord($char) - ord('A') - $shift + 26) % 26) + ord('A'));
            $j++;
        } else {
            $plainChar = $char;
        }

        $plaintext .= $plainChar;
    }

    return $plaintext;
}
?>
